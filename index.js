console.log("Hello world!");
// enclose with quotation mark string datas

console. log("Hello world!");
// case sensitive

console.
log
(
	"Hello, everyone!"
)

// ; delimeter
// we use delimeter to end code

// [COMMENTS]
//  - single line comments
//  Shortcut: ctrl + /

//  I am a single line comment

/* Multi-line comment */
/*
	I am
	a
	multi line
	comment
*/
/* shortcut: ctrl + shift + / */


// Syntax and statement

// Statements in programming are instructions that we tell compluter to perform
// Syntax in programming is the set of rules that describes how statements must be considered

// Variables
/*
	it is used to contain data
	
	-Syntax in declaring variables
	- let/const variableName
*/

let myVariable = "Hello";
			// assignment operator (=;)
console.log(myVariable);

let temperature = 82;

// console.log(hello); // will result to not defined error

/*
	Guides in writing variables:
		1. Use the let 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		2. Variable names should start a lowercase character, use a camelCase for multiple words.
		3. For constant variable, use the const keyword
		4. Variable names should be indicative or descriptive of the value being stored.
		5. Never name a variable starting with numbers
		6. Refrain from using space in declaring a variable

*/

// String
let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

// Number
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest)

// Reassignning variable value
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.89;

const pi = 3.14;
// pi = 3.16;

// Reassigning - a variable already have a value and we reassign a new one
// Initializing - it is a first saving of a value

let supplier;  // declaration
supplier = "John SMith Trading";  // initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable a reserve keyword
// const let = "hello";
// // console.log(let);

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// Strings in JavaScript is enclosed with single (' ') or double (" ")quote
let country = 'Philippines';
let province = "Metro Manila";

console.log(province + ', ' + country);
// We use  + symbol to concatenate data / values
let fullAddress = province + ', ' + country;
console.log(fullAddress)

console.log("Philippines" + ', ' + "Metro Manila")


// Escape Character (\) 
// "\n" refers to creating a new line or set the text to the next line

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilppines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

message = 'John\'s employee went home early';
console.log(message);

// Numbers

//  Integers / Whole number
let headcount = 26;
console.log(headcount);


// Decimal numbers / float / fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10
console.log(planetDistance);

console.log("John's grade last quarter is "+ grade);


// Arrays
// it is used to store multiple values with simila data type
let grades = [98.7, 95.4, 90.2, 94.6];
console.log(grades);

// different data types
// Storing different data types inside an array is not recommended because it will not make sense in the context of programming
let details = ["John", "Smith", 32, true];
	// arrayName //elements
console.log(details)

// Objects
// Objects are another special kind of data type that is used to mimic real world objects/items.

// Syntax:
/*
	let/const objectName = {
	propertytyA: value,
	propertytyB: value,
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	// key/property  // value
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 744"],
	// address: {
	// 	houseNumber: "345",
	// 	city: "Manila"
	// }
};

console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6,
};

console.log(myGrades);

let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as object

// 'type of' -
// we use type of operator to retrieve / know the data type 

console.log(typeof stringValue); // output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); // output: boolean
console.log(typeof waterBills); // output: object
console.log(typeof myGrades); //output: object

// Contant objects and Arrays
// We cannot re-assign the value of the variable, but we can change the elements of constant array
const anime = ["Boruto", "One Piece", "Code Geas", "Monster", "Dan Maci", "Demon Slayer", "AOT", "Fairy Tale"];
// index - is the position of the element starting zero. 0 1 2 3 and so on
anime[0] = "Naruto";
console.log(anime);

// Null
// It is used to intentionally express the absence of a value
let spouse = null;
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared but withiut an assigned value
let fullName; // declaration
console.log(fullName);




